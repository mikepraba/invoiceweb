library query_service;

import 'dart:async';

import 'package:angular/angular.dart';
import 'package:InvoiceWeb/component/invoice_grid.dart';

@Injectable()
class QueryService {
  String _invoicesUrl = 'invoices.json';

  Future _loaded;

  Map<int, Invoice> _invoicesCache;

  final Http _http;

  QueryService(Http this._http) {
    _loaded = Future.wait([_loadInvoices()]);
  }

  Future _loadInvoices() {
    return _http.get(_invoicesUrl)
      .then((HttpResponse response) {
        _invoicesCache = new Map<int, Invoice>();
        for (Map invoice in response.data) {
          Invoice inv = new Invoice.fromJson(invoice);
          _invoicesCache[inv.id] = inv;
        }
      });
  }

  Invoice getInvoiceById(int id) {
    return _invoicesCache != null
        ? _invoicesCache[id]
        : null;
  }

  Future<Map<int, Invoice>> getAllInvoices() {
    return _invoicesCache == null
        ? _loaded.then((_) => _invoicesCache)
        : new Future.value(_invoicesCache);
  }

  void updateInvoice(Invoice invoice) {
    _invoicesCache[invoice.id] = invoice;
  }

  void createInvoice(Invoice invoice) {
    if (invoice.id ==0 ) {
      int maxId = 0;
      for (int id in _invoicesCache.keys) {
        maxId > id ? maxId : id;
      }
      _invoicesCache[maxId + 1] = invoice;
    } else{
      //existing id needs to be handled later
      _invoicesCache[invoice.id] = invoice;
    }
  }

  void deleteInvoice(int invoiceId) {

    if(_invoicesCache.containsKey(invoiceId)) {
      _invoicesCache.remove(invoiceId);
    }
  }
}
