library recipe_book;

import 'package:angular/angular.dart';
import 'package:angular/application_factory.dart';
import 'package:InvoiceWeb/component/invoice_grid.dart';
import 'package:InvoiceWeb/component/invoice_add.dart';
import 'package:InvoiceWeb/component/invoice_edit.dart';
import 'package:InvoiceWeb/routing/invoice_grid_router.dart';
import 'package:InvoiceWeb/service/query.dart';

class MyAppModule extends Module {
  MyAppModule() {
    bind(InvoiceGridComponent);
    bind(InvoiceAddComponent);
    bind(InvoiceEditComponent);
    bind(QueryService);
    bind(RouteInitializerFn, toValue: invoiceGridRouteInitializer);
    bind(NgRoutingUsePushState, toValue: new NgRoutingUsePushState.value(false));
  }
}

void main() {
  applicationFactory()
      .addModule(new MyAppModule())
      .run();
}
