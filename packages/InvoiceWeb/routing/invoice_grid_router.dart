library invoice_grid_routing;

import 'package:angular/angular.dart';

void invoiceGridRouteInitializer(Router router, RouteViewFactory views) {
  views.configure({
    'invoiceGrid' : ngRoute(
        path: '/listInvoices',
        view: "view/listInvoices.html"),
    'add': ngRoute(
        path: '/addInvoice',
        view: 'view/addInvoice.html'),
    'invoice': ngRoute(
          path: '/invoice/:invoiceId',
        mount: {
          'view': ngRoute(
              path: '/view',
              view: 'view/viewInvoice.html'),
          'edit': ngRoute(
              path: '/edit',
              view: 'view/editInvoice.html'),
          'view_default': ngRoute(
              defaultRoute: true,
              enter: (RouteEnterEvent e) =>
                  router.go('view', {},
                      startingFrom: router.root.findRoute('invoice'),
                      replace: true))
        })
  });
}