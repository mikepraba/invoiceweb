library invoice_edit_component;

import 'package:angular/angular.dart';
import 'package:InvoiceWeb/component/invoice_grid.dart';
import 'package:InvoiceWeb/service/query.dart';

@Component(
    selector : 'invoice-edit',
    templateUrl : 'invoice_edit.html'
)
class InvoiceEditComponent {

  final QueryService queryService;
  final Router router;

  Invoice invoice;

  Invoice get invoiceEdit => invoice;

  InvoiceEditComponent(RouteProvider routeProvider, this.router, this.queryService) {

    var str = routeProvider.parameters['invoiceId'];
    int _invoiceId = str != null ? int.parse(str) : 0;
    invoice = queryService.getInvoiceById(_invoiceId);
    if(invoice == null) {
      invoice = new Invoice(0, '', 0.0);
    }
  }

  void updateInvoice() {

    queryService.updateInvoice(invoiceEdit);
    router.gotoUrl('/listInvoices');
  }
}