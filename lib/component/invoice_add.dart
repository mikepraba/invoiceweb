library invoice_add_component;

import 'package:angular/angular.dart';
import 'package:InvoiceWeb/component/invoice_grid.dart';
import 'package:InvoiceWeb/service/query.dart';

@Component(
    selector : 'invoice-add',
    templateUrl : 'invoice_add.html'
)
class InvoiceAddComponent {

  final QueryService queryService;
  final Router router;

  Invoice invoice;

  Invoice get invoiceCreate => invoice;

  InvoiceAddComponent(this.router, this.queryService) {

    invoice = new Invoice(0, '', 0.0);
  }

  void updateInvoice() {

    queryService.createInvoice(invoiceCreate);
    router.gotoUrl('/listInvoices');
  }
}