library invoice_grid_component;

import 'package:angular/angular.dart';
import 'package:InvoiceWeb/service/query.dart';

@Component(
    selector : 'invoice-grid',
    templateUrl : 'invoice_grid.html'
)
class InvoiceGridComponent {

  static const String LOADING_MESSAGE = "Loading invoice list...";
  static const String ERROR_MESSAGE = "Sorry! The cook stepped out of the";

  List<Invoice> invoices;
  Map<int, Invoice> _invoicesMap;

  String message = LOADING_MESSAGE;
  bool invoicesLoaded;
  final QueryService queryService;

  String get text => "passed text";
  InvoiceGridComponent(this.queryService) {
    _loadData();
  }

  Map<int, Invoice> get invoicesMap {
    print(_invoicesMap ==  null ? 'null' : _invoicesMap.length);
    return _invoicesMap;
  }

  void _loadData() {
    queryService.getAllInvoices()
        .then((Map<int, Invoice> allInvoices) {
      _invoicesMap = allInvoices;
      invoices = _invoicesMap.values.toList();
      invoicesLoaded = true;
    })
        .catchError((e) {
      print(e);
      invoicesLoaded = false;
      message = ERROR_MESSAGE;
    });
  }

  void deleteInvoice(double invoiceId) {

    print("inside delte" + invoiceId.toString());
    //queryService.deleteInvoice(invoiceId);
    _loadData();
  }

}

class Invoice {
  int _id;
  final String invoiceNo;
  double amount;

  int get id => _id;


  Invoice(this._id, this.invoiceNo, this.amount);

  Invoice.fromJson(Map<String, dynamic> json): this(json['id'], json['invoiceNo'], json['amount']);
}